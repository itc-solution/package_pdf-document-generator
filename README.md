# Document Generator for courses

This package generates documents(diplomas, diploma additions and vouchers) based on the data supplied.

## Usage

When instancing DocumentGenerator class only type of document is needed.

e.g. `diploma` `diploma addition` `voucher`

`$generator = new DocumentGenerator($type);`

Function responsible for document generation

`$generator->generate_pdfs($data, $pdf_path, $output_dir_path)`

### PDF Generator function parameters

`$data` Data used to generate dynamic content for the document

`$pdf_path`  Path to the file which is used as a template for current document type. Dynamic content will be written
over this pdf document.

`$output_dir_path` Path to directory where all generated documents will be stored. It must exist.

### $data array keys

`$data` variable should contain array for each of the student for whom data is generated.

e.g.

```
'first_name' => $student->fname,
'last_name' => $student->lname,
'diploma_number' => $nextDiplomaNumber,
'diploma_addition_number' => $nextDiplomaAdditionNumber,
'number_of_classes' => $course->classes,
'course_period' => $coursePeriod,
'achievement_date' => $achievementDate
```

### Data needed for diploma generation only
```
'first_name' => $student->fname,
'last_name' => $student->lname,
'number_of_classes' => $course->classes,
'achievement_date' => $achievementDate
'diploma_number' => $nextDiplomaNumber,
```

### Data needed for diploma addition generation only
```
'first_name' => $student->fname,
'last_name' => $student->lname,
'diploma_number' => $nextDiplomaNumber,
'diploma_addition_number' => $nextDiplomaAdditionNumber,
'number_of_classes' => $course->classes,
'course_period' => $coursePeriod,
'achievement_date' => $achievementDate
```

### Data needed for voucher generation only
```
'first_name' => $student->fname,
'last_name' => $student->lname,
'achievement_date' => $achievementDate
```