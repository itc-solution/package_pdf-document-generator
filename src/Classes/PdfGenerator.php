<?php

namespace ItcSolution\PdfDocumentGenerator\Classes;

use setasign\Fpdi\Fpdi;

abstract class PdfGenerator
{
    protected $fpdi;

    public function __construct($pdf_file_path)
    {
        $this->open_and_bootstrap_pdf($pdf_file_path);
    }

    private function open_and_bootstrap_pdf($pdf_file_path)
    {
        $this->fpdi = new Fpdi();
        $this->fpdi->AddPage();
        $this->fpdi->setSourceFile($pdf_file_path);
        $imported_page_id = $this->fpdi->importPage(1);
        $this->fpdi->useTemplate($imported_page_id, 0, 0, null, null, true);
        $this->fpdi->AddFont('Novecento-WideDemiBold', '', 'Novecento-WideDemiBold.php');
        $this->fpdi->AddFont('Novecento-WideMedium', '', 'Novecento-WideMedium.php');
        $this->fpdi->AddFont('Novecento-WideNormal', '', 'Novecento-WideNormal.php');
        $this->fpdi->AddFont('DINPro', '', 'DINPro.php');
        $this->fpdi->AddFont('DINPro-Light', '', 'DINPro-Light.php');
    }

    public function get_fpdi_instance(): Fpdi
    {
        return $this->fpdi;
    }

    abstract public function write_name($first_name, $last_name);

    abstract public function write_achievement_date($date);

    abstract public function download($output_folder, $candidate_name);

    abstract public function print_content($data);
}