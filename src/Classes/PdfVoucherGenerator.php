<?php

namespace ItcSolution\PdfDocumentGenerator\Classes;

class PdfVoucherGenerator extends PdfGenerator
{
    public function __construct($pdf_file_path)
    {
        parent::__construct($pdf_file_path);
        $this->fpdi->SetTextColor(128, 129, 132);
    }

    public function write_name($first_name, $last_name)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '18');
        $this->fpdi->SetXY(5, 49);
        $this->fpdi->Cell(95, 10, mb_convert_encoding("$first_name $last_name", 'iso-8859-2', 'UTF-8'), 0, 1, 'C');
    }

    public function write_achievement_date($date)
    {
        $this->fpdi->SetFont('DINPro', '', '12');
        $this->fpdi->Text(22, 132.8, $date);
    }

    public function download($output_folder, $candidate_name)
    {
        $this->fpdi->Output("$output_folder/$candidate_name - Vaucer.pdf", 'F', true);
    }

    public function print_content($data)
    {
        $this->write_name($data['first_name'], $data['last_name']);
        $this->write_achievement_date($data['achievement_date']);
    }
}