<?php

namespace ItcSolution\PdfDocumentGenerator\Classes;

class PdfAdditionGenerator extends PdfGenerator
{
    public function __construct($pdf_file_path)
    {
        parent::__construct($pdf_file_path);
        $this->fpdi->SetTextColor(35, 31, 32);
    }

    public function write_name($first_name, $last_name)
    {
        $this->fpdi->SetFont('DINPro', '', '14');
        $this->fpdi->Text(
            80.5, 41,
            mb_convert_encoding(
                "$first_name $last_name",
                'iso-8859-2', 'UTF-8')
        );
    }

    public function write_number_of_classes($number)
    {
        $this->fpdi->SetFont('DINPro', '', '13');
        $this->fpdi->Text(80.2, 84.5, $number);
    }

    public function write_class_period($period)
    {
        $this->fpdi->SetFont('DINPro', '', '13');
        $this->fpdi->Text(129.2, 84.5, $period);
    }

    public function write_diploma_number($number)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '14');
        $this->fpdi->Text(16.7, 156.8, "#$number");
    }

    public function write_addition_number($number)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '14');
        $this->fpdi->Text(16.7, 233.5, "#$number");
    }

    public function write_achievement_date($date)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '14');
        $this->fpdi->Text(16.4, 252.5, $date);
    }

    public function download($output_folder, $candidate_name)
    {
        $this->fpdi->Output("$output_folder/$candidate_name - Dodatak.pdf", 'F', true);
    }

    public function print_content($data)
    {
        $this->write_name($data['first_name'], $data['last_name']);
        $this->write_number_of_classes($data['number_of_classes']);
        $this->write_class_period($data['course_period']);
        $this->write_diploma_number($data['diploma_number']);
        $this->write_addition_number($data['diploma_addition_number']);
        $this->write_achievement_date($data['achievement_date']);
    }
}