<?php

namespace ItcSolution\PdfDocumentGenerator\Classes;

define('FPDF_FONTPATH', __DIR__ . '/../fonts/');

class DocumentGenerator
{
    private $pdf_type;

    public function __construct($pdf_type)
    {
        $this->pdf_type = $pdf_type;
    }

    function generate_pdfs($data, $pdf_path, $output_dir_path)
    {
        foreach ($data as $documentData) {
            $pdfGenerator = $this->get_pdf_generator($pdf_path);
            $candidate_name = $documentData['first_name'] . ' ' . $documentData['last_name'];
            $pdfGenerator->print_content($documentData);

            $pdfGenerator->download($output_dir_path, $candidate_name);
        }
    }

    private function get_pdf_generator($pdf_path)
    {
        switch ($this->pdf_type) {
            case 'diploma' :
                return new PdfDiplomaGenerator($pdf_path);
            case 'addition' :
                return new PdfAdditionGenerator($pdf_path);
            case 'voucher' :
                return new PdfVoucherGenerator($pdf_path);
            default :
                return null;
        }
    }
}