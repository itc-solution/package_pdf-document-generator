<?php

namespace ItcSolution\PdfDocumentGenerator\Classes;

class PdfDiplomaGenerator extends PdfAdditionGenerator
{
    public function __construct($pdf_file_path)
    {
        parent::__construct($pdf_file_path);
        $this->fpdi->SetTextColor(147, 149, 152);
    }

    public function write_name($first_name, $last_name)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '32');
        $this->fpdi->SetXY(60, 80);
        $this->fpdi->Cell(142, 50, mb_convert_encoding($first_name, 'ISO-8859-2', 'UTF-8'), 0, 1, "C");
        $this->fpdi->SetXY(60, 92);
        $this->fpdi->Cell(142, 50, mb_convert_encoding($last_name, 'ISO-8859-2', 'UTF-8'), 0, 1, "C");
    }

    public function write_number_of_classes($number)
    {
        $positionX = in_array(($number % 10), [0,2,3,4]) ? 134 : 138;

        if($number == 155){
            $positionX = 134;
        }

        $this->fpdi->SetFont('Novecento-WideDemiBold', '', '16');
        $this->fpdi->SetXY($positionX, 203.5);
        $this->fpdi->Cell(17, 10, $number, 0, 1, "C");
    }

    public function write_achievement_date($date)
    {
        $this->fpdi->SetFont('Novecento-WideMedium', '', '16');
        $this->fpdi->Text(73.6, 242, $date);
    }

    public function write_diploma_number($number)
    {
        $number = "#$number";
        $this->fpdi->SetFont('Novecento-WideMedium', '', '16');
        $this->fpdi->Text(155.6, 242, $number);
    }

    public function download($output_folder, $candidate_name)
    {
        $this->fpdi->AddPage();
        $imported_page_id = $this->fpdi->importPage(2);
        $this->fpdi->useTemplate($imported_page_id, 0, 0, null, null, true);

        $this->fpdi->Output("$output_folder/$candidate_name - Diploma.pdf", 'F', true);
    }

    public function print_content($data)
    {
        $this->write_name($data['first_name'], $data['last_name']);
        $this->write_number_of_classes($data['number_of_classes']);
        $this->write_achievement_date($data['achievement_date']);
        $this->write_diploma_number($data['diploma_number']);
    }
}
